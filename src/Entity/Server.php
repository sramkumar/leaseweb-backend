<?php

declare(strict_types=1);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Server
 *
 * @author ramkumar
 */
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Filter\SearchAnnotation as Searchable;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      shortName="servers",
 *      normalizationContext={"groups"={"read"}},
 *      denormalizationContext={"groups"={"write"}},
 *      collectionOperations={
 *          "get"={
 *              "method"="GET",
 *          },
 *          "api_filters"={
 *              "method"="GET",
 *              "route_name"="api_filter_data",
 *              "filters"={"SearchFilter::class"}
 *          },
 *      },
 *      itemOperations={"get"={"method"="GET"}},
 *      attributes={
 *          "pagination_items_per_page"=10,
 *          "pagination_enabled"=true
 *      }
 * )
 * @ApiFilter(SearchFilter::class, properties={"ram": "exact","location": "exact","hddType": "exact"})
 */
class Server
{
    /**
     * @var int
     * @ApiProperty(identifier=true)
     * @Groups({"write"})
     */
    protected $id;
    
    /**
     * @var string
     * @Groups({"read"})
     */
    protected $model;
    
    /**
     * @var string
     * @Groups({"read"})
     */
    protected $ram;
    
    /**
     * @var string
     * @Groups({"write"})
     */
    protected $ramType;
    
    /**
     * @var string
     * @Groups({"read"})
     */
    protected $hdd;
            
    /**
     * @Groups({"write"})
     * @var string
     */
    protected $hddSize;
    
    /**
     * @Groups({"write"})
     * @var string
     */
    protected $hddType;
    
    /**
     * @Groups({"read","write"})
     * @var string
     */
    protected $location;
    
    /**
     * @Groups({"read","write"})
     * @var string
     */
    protected $currency;
    
    /**
     * @var string
     * @Groups({"read","write"})
     */
    protected $price;
    
    public function __construct($id, $model, $ram, $ramType, $hdd, $hddType, $hddSize, $location, $price, $currency)
    {
        $this->id = $id;
        $this->model = $model;
        $this->hdd = $hdd;
        $this->hddType = $hddType;
        $this->hddSize = $hddSize;
        $this->ram = $ram;
        $this->ramType = $ramType;
        $this->price = $price;
        $this->currency = $currency;
        $this->location = $location;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getModel() : string
    {
        return $this->model;
    }

    public function getRam() : string
    {
        return $this->ram;
    }

    public function getHdd() : string
    {
        return $this->hdd;
    }

    public function getHddSize() : string
    {
        return $this->hddSize;
    }
    
    public function getHddType() : string
    {
        return $this->hddType;
    }

    public function getLocation() : string
    {
        return $this->location;
    }
    
    public function getCurrency() : string
    {
        return $this->currency;
    }
    
    public function getPrice() : string
    {
        return $this->price;
    }
}
